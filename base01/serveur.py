from bottle import route, run, template, request


score = 0


@route("/")
def home():
    global score
    score = score + 1
    return template("templates/index.html",
        {"name": "TOTO", "score": score})


@route("/reponse", method=["POST"])
def reponse():
    print(request.forms.get("nom"))
    return template("templates/merci.html")
    

run(host='0.0.0.0', port=3001, reloader=True)