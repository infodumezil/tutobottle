from bottle import route, run, template, request, static_file


score = 0


@route("/")
def home():
    return template("templates/home.html", {})


@route("/play", method=["POST"])
def reponse():
    global score;
    score += int(request.forms.get("value"))
    return {"status": "OK", "points": score}



@route("/static/<filename:path>")
def static(filename):
    return static_file(filename, root="./static") 

run(host='0.0.0.0', port=3001, reloader=True) 